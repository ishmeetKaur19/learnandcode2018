namespace ITTDB_Test
{
    
    /// <summary>
    /// Class Test
    /// </summary>
    
    public class Test
    {
       public int id {get; set;}
       public string sampleName { get; set; }
       public string sampleAddress { get; set; }
       public void setAddress(string newAddress) {
            this.sampleAddress = newAddress; 
       }
    }
}
