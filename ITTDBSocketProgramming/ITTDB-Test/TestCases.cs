using Microsoft.VisualStudio.TestTools.UnitTesting;
using ITTDBServer;
using System;

namespace ITTDB_Test
{
    [TestClass]
    public class UnitTest1
    {
        ObjectManagement objectManagement = new ObjectManagement();

        [TestMethod]
        public void delete()
        {
            // Arrange
            Object actualResult = new { 
                id = 1,
                name = "The Magic of thinking big",
                authorName = "David S.schwartz",
                price = 2800.0};

            Object obj = new {
                id = 1,
                name = "The Magic of thinking big",
                authorName = "David S.schwartz",
                price = 2800.0
            };

            // Act
            object expectedResult = objectManagement.deleteObject("Test", obj);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void save()
        {
            // Arrange
            object actualResult = new {
                id = 1,
                name = "The Magic of thinking big",
                authorName = "David S.schwartz",
                price = 2800.0
            };
            Object obj = new {
                id = 0,
                name = "The Magic of thinking big",
                authorName = "David S.schwartz",
                price = 2800.0
            };

            // Act
            object expectedResult = objectManagement.saveObject("Test", obj );

            //Assert
            Assert.AreEqual(expectedResult, actualResult);

        }

        [TestMethod]
        public void modify()
        {
            // Arrange
            Object actualResult = new {
                id = 1,
                name = "The Magic of thinking big",
                authorName = "David S.schwartz",
                price = 3000.0
            };
            Object obj = new {
                id = 1,
                name = "The Magic of thinking big",
                authorName = "David S.schwartz",
                price = 3000.0
            };

            // Act
            object expectedResult = objectManagement.modifyObject("Test", obj);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);

        }
    }
}
