using System.Text;

namespace ITTDBClient
{
    class RequestResponse
    {
        
        ClientExecution clientExecution = new ClientExecution();
        serializationDeserializationUtility.serializationDeserializationLibrary sdl = new serializationDeserializationUtility.serializationDeserializationLibrary();
        public void createRequest(string operation, string dataType, object data)
        {
            var json = new {
                Operation = operation,
                DataType = dataType,
                Data = data 
            };
            
            string serializedData = sdl.serializeObject(json);            
            byte[] requestData = Encoding.ASCII.GetBytes(serializedData);
            clientExecution.executeClient(requestData);
        }
    }
}