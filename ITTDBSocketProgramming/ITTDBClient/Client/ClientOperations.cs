namespace ITTDBClient
{
    using System;
    using System.Net;
    using System.Net.Sockets;

    public class ClientOperations
    {
        static IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
        static IPAddress ipAddr = ipHost.AddressList[0];

        Socket sender ;

        public void InitializeSender()
        {
            sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }

        public Socket connect()
        {           
            this.InitializeSender();
            IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 11111);
            sender.Connect(localEndPoint);
            Console.WriteLine("Socket connected to -> {0} ",sender.RemoteEndPoint.ToString());
            return sender;
        }
    }

    
}