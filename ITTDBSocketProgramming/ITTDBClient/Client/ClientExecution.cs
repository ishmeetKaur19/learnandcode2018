namespace ITTDBClient
{
    using System;
    using System.Net.Sockets;

    public class ClientExecution
    {
        ClientOperations clientOperations = new ClientOperations();
        socketUtility.socketOperationsLibrary socketLibrary = new socketUtility.socketOperationsLibrary();
        public void executeClient(Byte[] requestData)
        {
            Socket sender = clientOperations.connect();

            Console.WriteLine("Sending Request.....");

            socketLibrary.send(sender, requestData);

            Console.WriteLine("Receiving data.....");

            string receivedData = socketLibrary.receive(sender);

            Console.WriteLine("Received Response {0}", receivedData);

            socketLibrary.disconnect(sender);

            Console.WriteLine("Disconnected");

        }

    }
}