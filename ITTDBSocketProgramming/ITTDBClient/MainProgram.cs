﻿using System;

namespace ITTDBClient
{
    class Program
    {
        static void Main(string[] args)
        {
           Customer customer = new Customer();
           customer.name = "Ishmeet";
           customer.address = "Kota";
           Book book = new Book();
           book.name = "The Magic of thinking big";
           book.price = 2800.0;
           book.authorName = "David S.schwartz";
          
           RequestResponse requestResponse = new RequestResponse();
           Object objectToBeFind = new {
               
           };
           Object objectToBeDeleted = new {
               id = 1
           };

            customer.setAddress("newAddress");
            customer.id = 1;

            requestResponse.createRequest("Save", "Customer", customer);
            requestResponse.createRequest("Save", "Book", book); 
            // requestResponse.createRequest("Find", "Customer", objectToBeFind);
            // requestResponse.createRequest("Delete", "Book", objectToBeDeleted);
            // requestResponse.createRequest("Update", "Customer", customer);
            
        }
    }
}
