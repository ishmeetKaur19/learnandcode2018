namespace ITTDBClient
{  
    /// <summary>
    /// Class Customer
    /// </summary>
    class Customer
    {
       public int id {get; set;}
       public string name { get; set; }
       public string address { get; set; }
       public void setAddress(string newAddress) {
            this.address = newAddress; 
       }
    }
}