﻿
namespace serializationDeserializationUtility
{
    #region Namespaces

    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using System.IO;

    #endregion Namespaces

    public class serializationDeserializationLibrary
    {

        public string serializeList(List<object> objectList)
        {
            string serializedData = JsonConvert.SerializeObject(objectList);
            return serializedData;
        }


        public List<Object> deserializeFileData(string filename)
        {
            string fileData;
            FileStream fileStream = File.OpenRead(filename);
            using (StreamReader streamReader = new StreamReader(fileStream))
            {
                fileData = streamReader.ReadLine();
                fileStream.Close();
            }
            return JsonConvert.DeserializeObject<List<Object>>(fileData);
        }

        public string serializeObject(Object obj)
        {
            string serializedData = JsonConvert.SerializeObject(obj);
            return serializedData;
        }

        public object deserializingString(string serializedData)
        {
            return JsonConvert.DeserializeObject<Object>(serializedData);
        }

    }
}