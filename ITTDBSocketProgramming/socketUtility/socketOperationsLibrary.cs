﻿

namespace socketUtility
{
    #region Namespaces

    using System;
    using System.Net.Sockets;
    using System.Text;

    #endregion Namespaces
    public class socketOperationsLibrary
    {
        public void disconnect(Socket socket)
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }

        public void send(Socket socket, byte[] message)
        {
            socket.Send(message);
        }

        public string receive(Socket socket)
        {

            byte[] bytes = new Byte[socket.ReceiveBufferSize];

            int byteReceived = socket.Receive(bytes);
            string data = Encoding.ASCII.GetString(bytes, 0, byteReceived);

            return data; 
        }

    }
}
