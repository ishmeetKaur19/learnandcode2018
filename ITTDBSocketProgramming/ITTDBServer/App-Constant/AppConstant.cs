namespace ITTDBServer
{
    public static class AppConstants
    {
        public const string SaveError = "SAVE_ERROR";
        public const string SaveSuccess = "SAVE_SUCCESS";
        public const string FindError = "FIND_ERROR";
        public const string FindSuccess = "FIND_SUCCESS";
        public const string DeleteError = "DELETE_ERROR";
        public const string DeleteSuccess = "DELETE_SUCCESS";
        public const string UpdateError = "UPDATE_ERROR";
        public const string UpdateSuccess = "UPDATE_SUCCESS";
        public const string Ok = "200";
        public const string BadRequest = "400";
        public const string InternalServerError = "500";
        public const string Created = "201";
        public const string DataNotNull = "Data should not be null";
        public const string FileNotExistEmpty = "File not exist or file is empty.";
        public const string ObjectNotFound = "Object not found or file not exist or file is empty.";
        public const string Save = "Save";
        public const string Delete = "Delete";
        public const string Update = "Update";
        public const string Find = "Find";
    }
}