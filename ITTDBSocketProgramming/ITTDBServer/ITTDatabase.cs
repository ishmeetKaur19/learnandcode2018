
namespace ITTDBServer
{
    #region Namespaces

    using System;

    #endregion Namespaces

    /// <summary>
    /// Class ITTDatabase contains CURD operations.
    /// </summary>
    public class ITTDatabase 
    {
        ObjectManagement objectManagement = new ObjectManagement();
        ResponseManagement responseManagement = new ResponseManagement();

        string response;
        string status;
        
        public string Save(string objectType, Object obj)
        {
            try
            {
                if (obj == null)
                {
                    response = responseManagement.createFailureResponse(AppConstants.SaveError, AppConstants.DataNotNull, AppConstants.BadRequest);
                }

                Object savedObject = objectManagement.saveObject(objectType, obj);
                response = responseManagement.createSuccessResponse(savedObject, AppConstants.SaveSuccess, AppConstants.Created);
            }

            catch (Exception exception)
            {
                 response = responseManagement.createFailureResponse(AppConstants.SaveError, exception.Message, AppConstants.InternalServerError);
            }
            return response;

        }

        public string Find(string objectType, Object obj)
        {

            try
            {
                if (obj == null)
                {
                    response = responseManagement.createFailureResponse(AppConstants.FindError, AppConstants.DataNotNull, AppConstants.BadRequest);
                }
                
                Object objectFound = objectManagement.findObject(objectType, obj);
                if (objectFound != null)
                {
                    status = AppConstants.FindSuccess;
                }
                else
                {
                    status = AppConstants.ObjectNotFound;

                }
                response = responseManagement.createSuccessResponse(objectFound, status, AppConstants.Ok);

            }

            catch (Exception exception)
            {
                response = responseManagement.createFailureResponse(AppConstants.FindError, exception.Message, AppConstants.InternalServerError);     
            }

            return response;

        }

        public string Delete(string objectType, Object obj)
        {

            try
            {
                if (obj == null)
                {
                    response = responseManagement.createFailureResponse(AppConstants.DeleteError, AppConstants.DataNotNull, AppConstants.BadRequest);
                }

                Object deletedObject =  objectManagement.deleteObject(objectType, obj);
                if (deletedObject != null)
                {
                    status = AppConstants.DeleteSuccess;
                }
                else
                {
                    status = AppConstants.FileNotExistEmpty;
                }
                response = responseManagement.createSuccessResponse(deletedObject, status, AppConstants.Ok);
            }

            catch (Exception exception)
            {
                response = responseManagement.createFailureResponse(AppConstants.DeleteError, exception.Message, AppConstants.InternalServerError);
            }

            return response;
        }

        public string Update(string objectType, object obj)
        {
            
            try
            {
                if (obj == null)
                {
                    response = responseManagement.createFailureResponse(AppConstants.UpdateError, AppConstants.DataNotNull, AppConstants.BadRequest);
                }

                Object modifiedObject = objectManagement.modifyObject(objectType, obj);
                if (modifiedObject != null)
                {
                    status = AppConstants.UpdateSuccess;
                }
                else
                {
                    status = AppConstants.FileNotExistEmpty;
                }
                response = responseManagement.createSuccessResponse(modifiedObject, status, AppConstants.Ok);
                
            }

            catch (Exception exception)
            {
                response = responseManagement.createFailureResponse(AppConstants.UpdateError, exception.Message, AppConstants.InternalServerError);
            }

            return response;
        }
    }
}


