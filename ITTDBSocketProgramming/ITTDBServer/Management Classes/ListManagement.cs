namespace ITTDBServer
{
    #region Namespaces

    using System.Collections.Generic;

    #endregion Namespaces

    
    class ListManagement {

        public static List<object> appendObjectListWithNewObject(List<object> objectList, object newObjectToBeStored)
        {
            objectList.Add(newObjectToBeStored);
            return objectList;
        }

        public static List<object> removeObjectFromObjectList(List<object> objectList, object objectToBeRemoved) {
            objectList.Remove(objectToBeRemoved);
            return objectList;

        }


    } 
}