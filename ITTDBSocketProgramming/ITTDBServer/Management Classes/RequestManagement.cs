
namespace ITTDBServer
{
    #region Namespaces

    using Newtonsoft.Json.Linq;

    #endregion Namespaces

    class RequestManagement
    {
        ITTDatabase database = new ITTDatabase();

        public string processRequest(string requestData)
        {
             string response;
             object obj = JObject.Parse(requestData);
             JObject jObject = JObject.FromObject(obj);
             string operation = (string)jObject["Operation"];
             string dataType = (string)jObject["DataType"];
             object data = (object)jObject["Data"];
             switch(operation)
             {
                case AppConstants.Save:    
                    response = database.Save(dataType, data);
                    break;
                case AppConstants.Find:
                    response = database.Find(dataType, data);
                    break;
                case AppConstants.Delete:
                    response = database.Delete(dataType, data);
                    break;
                case AppConstants.Update:
                    response = database.Update(dataType, data);
                    break;
                default:
                    response = "Invalid Operation";
                    break;           
             }
            return response; 
        }
       
    }
}