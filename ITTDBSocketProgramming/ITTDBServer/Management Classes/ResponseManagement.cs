
namespace ITTDBServer
{
    #region Namespaces

    using serializationDeserializationUtility;

    #endregion Namespaces

    class ResponseManagement
    {
        serializationDeserializationLibrary serializationDeserializationLibrary = new serializationDeserializationLibrary();
        public string createSuccessResponse(object data, string status, string code)
        {
            var response = new {
                 Code = code,
                 Status = status,
                 Data = data
            };
            string serializedResponse = serializationDeserializationLibrary.serializeObject(response);
            return serializedResponse;
        }

        public string createFailureResponse(string status, string errorMessage, string errorCode)
        {
            var response = new {
                ErrorCode = errorCode,
                Status = status,
                ErrorMessage = errorMessage
            };

            string serializedResponse = serializationDeserializationLibrary.serializeObject(response);
            return serializedResponse;
        }
    }
}