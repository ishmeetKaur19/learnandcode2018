namespace ITTDBServer
{
    #region Namespaces

    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json.Linq;
    using serializationDeserializationUtility;

    #endregion Namespaces

    public class ObjectManagement
    {
        serializationDeserializationLibrary serializationDeserializationLibrary = new serializationDeserializationLibrary();

        public object saveObject(string objectType, Object obj)
        {
            List<Object> list = new List<Object>();
            string filePath = FileManagement.getFilePath(objectType);

            if (FileManagement.isFileExist(filePath))
            {
                if (!FileManagement.isFileEmpty(filePath))
                {
                    list = serializationDeserializationLibrary.deserializeFileData(filePath);
                }
            }

            object objectToBeSaved = setIdOfNewObject(list, obj);
            list = ListManagement.appendObjectListWithNewObject(list, objectToBeSaved);
            string serializedData = serializationDeserializationLibrary.serializeList(list);
            FileManagement.writeSerializedListIntoFile(filePath, serializedData);
            return objectToBeSaved;
        }

        public object findObject(string objectType, Object obj)
        {
            Object listObject = null;
            JObject jObject = JObject.FromObject(obj);
            string key = (string)jObject["key"];
            string expectedValue = (string)jObject["value"];
            string filePath = FileManagement.getFilePath(objectType);

            if (FileManagement.isFileExist(filePath))
            {
                if (!FileManagement.isFileEmpty(filePath))
                {
                    List<Object> list = serializationDeserializationLibrary.deserializeFileData(filePath);
                    listObject = list.Find(x => (string)JObject.FromObject(x)[key] == expectedValue);
                    if (listObject != null)
                    {
                        Console.WriteLine(listObject);
                        return listObject;
                    }
                }
            }
            return null;
        }

        public object deleteObject(string objectType, Object obj)
        {
            Object listObject;
            JObject jObject = JObject.FromObject(obj);
            int id = (int)jObject["id"];
            string filePath = FileManagement.getFilePath(objectType);

            if (FileManagement.isFileExist(filePath))
            {
                if (!FileManagement.isFileEmpty(filePath))
                {
                    List<Object> list = serializationDeserializationLibrary.deserializeFileData(filePath);
                    listObject = list.Find(x => (int)JObject.FromObject(x)["id"] == id);
                    if (listObject != null)
                    {
                        list = ListManagement.removeObjectFromObjectList(list, listObject);
                        string serializedData = serializationDeserializationLibrary.serializeList(list);
                        FileManagement.writeSerializedListIntoFile(filePath, serializedData);
                        return listObject;
                    }
                }
            }
            return null;
        }

        public object modifyObject(string objectType, object obj)
        {
            Object listObject;
            int listObjectIndex = 0;

            string filePath = FileManagement.getFilePath(objectType);

            if (FileManagement.isFileExist(filePath))
            {
                if (!FileManagement.isFileEmpty(filePath))
                {
                    List<Object> list = serializationDeserializationLibrary.deserializeFileData(filePath);
                    listObject = list.Find(x => (int)JObject.FromObject(x)["id"] == (int)JObject.FromObject(obj)["id"]);
                    listObjectIndex = list.FindIndex(x => (int)JObject.FromObject(x)["id"] == (int)JObject.FromObject(obj)["id"]);
                    if (listObject != null)
                    {
                        list = ListManagement.removeObjectFromObjectList(list, listObject);
                        list.Insert(listObjectIndex, obj);
                        string serializedData = serializationDeserializationLibrary.serializeList(list);
                        FileManagement.writeSerializedListIntoFile(filePath, serializedData);
                        return obj;
                    }
                }
            }
            return null;
        }

        public static object setIdOfNewObject(List<object> objectList, object newObjectToStore)
        {
            
            JObject jObject = JObject.FromObject(newObjectToStore);
            if (objectList.Count == 0)
            {
                jObject["id"] = 1;
            }
            else
            {
                Object lastObject = objectList[objectList.Count - 1];
                JObject listJObject = JObject.FromObject(lastObject);
                int id = (int)listJObject["id"];
                jObject["id"] = id+1;
            }
            return jObject;

        }
    }
}