using System;
using System.Net;
using System.Net.Sockets;

namespace ITTDBServer
{
    class ServerOperations
    {
        static IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
        static IPAddress ipAddr = ipHost.AddressList[0];
        static IPEndPoint localEndPoint = new IPEndPoint(ipAddr, 11111);
        Socket listener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

        public Socket start()
        {
            listener.Bind(localEndPoint);
            listener.Listen(0);
            return listener;
        }

        public Socket accept(Socket listener)
        {
            return listener.Accept();
        }
        
    }
}