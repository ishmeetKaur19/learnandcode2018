using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ITTDBServer
{
    class ServerExecution
    {
        ServerOperations serverOperations = new ServerOperations();
        RequestManagement requestManagement = new RequestManagement();
        socketUtility.socketOperationsLibrary socketLibrary = new socketUtility.socketOperationsLibrary();
        private Object _lock = new Object();

        public void executeServer()
        {
            Socket listener = serverOperations.start();
            Console.WriteLine("Server is started");
            int i = 1;
            while (true)
            {

                Socket clientSocket = serverOperations.accept(listener);

                Console.WriteLine("Connected to new client {0}", i++);

                // Multi-Threading
                Task task = new Task(executeClientRequest, clientSocket);

                task.Start();
            }
        }

        public void executeClientRequest(object clientSocket)
        {
            lock (_lock)
            {
                Socket socket = (Socket)clientSocket;
                string dataReceived = socketLibrary.receive(socket);

                Console.WriteLine("Request received from client {0}", dataReceived);

                Console.WriteLine("Press Enter to start the processing");
                Console.ReadLine();

                Console.WriteLine("Processing starts......");

                string response = requestManagement.processRequest(dataReceived);

                byte[] message = Encoding.ASCII.GetBytes(response);

                Console.WriteLine("sending response...");

                socketLibrary.send(socket, message);

                Console.WriteLine("Its completed.");

            }

        }
    }
}