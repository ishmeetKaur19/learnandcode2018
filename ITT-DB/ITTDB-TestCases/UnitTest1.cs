using System;
using ITT_Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;

namespace ITT_DB
{
    [TestClass]
    public class UnitTest
    {
        IITTDatabase ittDb = new ITTDatabase();
        
        [TestMethod]
        public void findById()
        {
            // Arrange
            Test test = new Test();
            test.id = 1;
            test.sampleName = "Siddharth";
            test.sampleAddress = "Ghaziabad";

            // Act
            JObject jObj = (JObject)ittDb.Find(test, "id","1");
            Test testObj = jObj.ToObject<Test>();

            // Assert
             Assert.AreEqual(test.id, testObj.id);

        }

        [TestMethod]
        public void findByName()
        {
            // Arrange
            Test test = new Test();
            test.id = 1;
            test.sampleName = "Siddharth";
            test.sampleAddress = "Ghaziabad";

            // Act
            JObject jObj = (JObject)ittDb.Find(test, "sampleName","Siddharth");
            Test testObj = jObj.ToObject<Test>();

            // Assert
             Assert.AreEqual(test.sampleName, testObj.sampleName);

        }

        [TestMethod]
        public void save()
        {
            // Arrange
            Boolean actualResult = true;
            Test test = new Test();
            test.sampleName = "Siddharth";
            test.sampleAddress = "Ghaziabad";

            // Act
            Boolean expectedResult = ittDb.Save(test);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);

        }

        [TestMethod]
        public void delete()
        {
            // Arrange
            Boolean actualResult = true;
            Test test = new Test();

            // Act
            Boolean expectedResult = ittDb.Delete(test, 1);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);

        }

        [TestMethod]
        public void modify()
        {
             // Arrange
            Boolean actualResult = true;
            Test test = new Test();
            test.id = 1;
            test.sampleName = "Siddharth";
            test.sampleAddress = "new Ghaziabad";

            // Act
            Boolean expectedResult = ittDb.Modify(test);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);

        }
    }
}
