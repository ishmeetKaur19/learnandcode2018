


namespace ITT_Database
{
    #region Namespaces

    using System;
    using ITT_Database;

    #endregion Namespaces

    /// <summary>
    /// Class Book
    /// </summary>
    class Book
    {
       public int id { get; set; }
       public string name { get; set; }
       public string authorName { get; set; }
       public double price { get; set; }
       public void setAuthorName(string newAuthorName) {
            this.authorName = newAuthorName; 
       }
    }
}
