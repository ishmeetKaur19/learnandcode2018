namespace ITT_Database
{
    #region Namespaces

    using System;
    using ITT_Database;

    #endregion Namespaces
    
    /// <summary>
    /// Class Test
    /// </summary>
    public class Test
    {
       public int id {get; set;}
       public string sampleName { get; set; }
       public string sampleAddress { get; set; }
       public void setAddress(string newAddress) {
            this.sampleAddress = newAddress; 
       }
    }
}
