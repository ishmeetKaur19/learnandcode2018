#region Namespaces

using ITT_Database;
using System;

#endregion Namespaces
class main
{
    /// <summary>
    /// Entry point of application
    /// </summary>
    /// <param name="args"></param>
    static void Main(string[] args)
    {
        AppDomain currentAppDomain = AppDomain.CurrentDomain;
        currentAppDomain.UnhandledException += new UnhandledExceptionEventHandler(HandleException);

        Customer customer = new Customer();
        customer.name = "Ishmeet";
        customer.address = "Kota";
        Customer customer1 = new Customer();
        customer1.name = "Nandita";
        customer1.address = "Jaipur";
        Customer customer2 = new Customer();
        customer2.name = "Mohit";
        customer2.address = "Agra";
        Customer customer3 = new Customer();
        customer3.name = "Sagar";
        customer3.address = "Jaipur";

        Book book = new Book();
        book.name = "The Magic of thinking big";
        book.authorName = "David S.schwartz";
        book.price = 2800;
        Book book1 = new Book();
        book1.name = "The Saint, The Surfer and The C.E.O.";
        book1.authorName = "Robin Sharma";
        book1.price = 3000;

        IITTDatabase ittDb = new ITTDatabase();

        ittDb.Save(customer);
        ittDb.Save(customer1);
        ittDb.Save(customer2);
        ittDb.Save(customer3);
        ittDb.Save(book);
        ittDb.Save(book1);

        Object bookObj = ittDb.Find(book, "name", "The Saint, The Surfer and The C.E.O.");
        if (bookObj != null)
        {
            Console.WriteLine(bookObj);
        }
        else
        {
            Console.WriteLine("Book not found");
        }
        Console.Read();

        Object customerObj = ittDb.Find(customer, "address", "Jaipur");
        if (customerObj != null)
        {
            Console.WriteLine(customerObj);
        }
        else
        {
            Console.WriteLine("Customer not found");
        }
        Console.Read();

        bool isBookDeleted = ittDb.Delete(book, 2);
        if (isBookDeleted)
        {
            Console.WriteLine("Book deleted successfully");
        }
        else
        {
            Console.WriteLine("Sorry we are not able to delete it, Book not found");
        }
        Console.Read();

        bool isCustomerDeleted = ittDb.Delete(customer, 3);
        if (isCustomerDeleted)
        {
            Console.WriteLine("Customer deleted successfully");
        }
        else
        {
            Console.WriteLine("Sorry we are not able to delete it, Customer not found");
        }
        Console.Read();

        customer1.setAddress("newAddress");
        customer1.id = 2;
        bool isCustomerModify = ittDb.Modify(customer1);
        if (isCustomerModify)
        {
            Console.WriteLine("Customer is modified successfully");
        }
        else
        {
            Console.WriteLine("Customer not modified");
        }
        Console.Read();
    }
    private static void HandleException(object sender, UnhandledExceptionEventArgs e)
    {
        Console.WriteLine("There was a problem and we need to close. You can see the Details : " + e.ExceptionObject);
    }
}