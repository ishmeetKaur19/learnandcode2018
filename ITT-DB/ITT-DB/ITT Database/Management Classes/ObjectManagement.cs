namespace ITT_Database
{
    #region Namespaces

    using System;
    using System.IO;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using Newtonsoft.Json.Linq;
    using ITT_Database;
    using System.Reflection;

    #endregion Namespaces

    class ObjectManagement
    {
        string fileName;
        ISerializingDeserializingManagement serializeDeserialize = new SerializingDeserializingManagement();
        public void saveObject(Object obj)
        {

            List<Object> list = new List<Object>();

            fileName = FileManagement.getFileName(obj);
            string filePath = FileManagement.getFilePath(fileName);

            if (FileManagement.isFileExist(filePath))
            {
                if (!FileManagement.isFileEmpty(filePath))
                {
                    list = serializeDeserialize.deserializing(filePath);
                }
            }

            setIdOfNewObject(list, obj);
            list = ListManagement.appendObjectListWithNewObject(list, obj);
            string serializedData = serializeDeserialize.serializing(list);
            FileManagement.writeSerializedListIntoFile(filePath, serializedData);
        }

        public object findObject(object instance, string key, string expectedValue)
        {
            Object listObject;
            fileName = FileManagement.getFileName(instance);
            string filePath = FileManagement.getFilePath(fileName);

            if (FileManagement.isFileExist(filePath))
            {
                if (!FileManagement.isFileEmpty(filePath))
                {
                    List<Object> list = serializeDeserialize.deserializing(filePath);
                    listObject = list.Find(x => (string)JObject.FromObject(x)[key] == expectedValue);
                    if (listObject != null)
                    {
                        return listObject;
                    }
                }
            }
            return default(object);
        }

        public bool deleteObject(object instance, int id)
        {
            Object listObject;
            fileName = FileManagement.getFileName(instance);
            string filePath = FileManagement.getFilePath(fileName);

            if (FileManagement.isFileExist(filePath))
            {
                if (!FileManagement.isFileEmpty(filePath))
                {
                    List<Object> list = serializeDeserialize.deserializing(filePath);
                    listObject = list.Find(x => (int)JObject.FromObject(x)["id"] == id);
                    if (listObject != null)
                    {
                        list = ListManagement.removeObjectFromObjectList(list, listObject);
                        string serializedData = serializeDeserialize.serializing(list);
                        FileManagement.writeSerializedListIntoFile(filePath, serializedData);
                        return true;
                    }
                }
            }
            return false;
        }

        public bool modifyObject(object obj)
        {

            Object listObject;
            int listObjectIndex = 0;

            fileName = FileManagement.getFileName(obj);
            string filePath = FileManagement.getFilePath(fileName);

            if (FileManagement.isFileExist(filePath))
            {
                if (!FileManagement.isFileEmpty(filePath))
                {
                    List<Object> list = serializeDeserialize.deserializing(filePath);
                    listObject = list.Find(x => (int)JObject.FromObject(x)["id"] == (int)JObject.FromObject(obj)["id"]);
                    listObjectIndex = list.FindIndex(x => (int)JObject.FromObject(x)["id"] == (int)JObject.FromObject(obj)["id"]);
                    if (listObject != null)
                    {
                        list = ListManagement.removeObjectFromObjectList(list, listObject);
                        list.Insert(listObjectIndex, obj);
                        string serializedData = serializeDeserialize.serializing(list);
                        FileManagement.writeSerializedListIntoFile(filePath, serializedData);
                        return true;
                    }
                }
            }
            return false;
        }

        public static void setIdOfNewObject(List<object> objectList, object newObjectToStore)
        {
            PropertyInfo objectProperty = newObjectToStore.GetType().GetProperty("id");
            if (objectList.Count == 0)
            {
                objectProperty.SetValue(newObjectToStore, 1, null);
            }
            else
            {
                Object lastObject = objectList[objectList.Count - 1];
                JObject jObject = JObject.FromObject(lastObject);
                int id = (int)jObject["id"];
                objectProperty.SetValue(newObjectToStore, id + 1, null);
            }

        }
    }
}