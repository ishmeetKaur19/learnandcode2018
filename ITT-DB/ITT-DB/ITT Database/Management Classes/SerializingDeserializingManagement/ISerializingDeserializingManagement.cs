using System.Collections.Generic;

namespace ITT_Database 
{
    
    public interface ISerializingDeserializingManagement
    {
        string serializing(List<object> objectList);

        List<object> deserializing(string filename);

    }
}