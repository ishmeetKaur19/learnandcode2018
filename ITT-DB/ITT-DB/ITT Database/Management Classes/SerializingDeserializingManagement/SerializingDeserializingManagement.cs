namespace ITT_Database
{
    #region Namespaces

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    #endregion Namespaces

     class SerializingDeserializingManagement: ISerializingDeserializingManagement {

       
        public string serializing(List<object> objectList)
        {
            string serializedData = JsonConvert.SerializeObject(objectList);
            return serializedData;     
        }

        
        public List<Object> deserializing(string filename)
        {
            string fileData = FileManagement.readingDataFromFile(filename);
            return JsonConvert.DeserializeObject<List<Object>>(fileData);
        }
 

    }
}
