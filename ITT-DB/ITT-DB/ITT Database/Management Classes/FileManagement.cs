namespace ITT_Database
{
    #region Namespaces

    using System;
    using System.IO;

    #endregion Namespaces

    public class FileManagement
    {
        public static string getFileName(object obj) {
            string fileName = obj.GetType().Name;
            return fileName;
        }

        public static bool isFileExist(string filePath) {
            return System.IO.File.Exists(filePath);
        }

        public static string getFilePath(string fileName) {
            return @"C:\Users\ishmeet.kaur\ITT-DB\" + fileName;
        }

        public static void createFile(string filePath) {
            System.IO.File.Create(filePath);
        }

        public static bool isFileEmpty(string filePath) {
            return (new FileInfo(filePath).Length == 0) ? true : false;
        }

        public static void writeSerializedListIntoFile(string fileName, string serializedData)
        {
            using (StreamWriter streamWriter = File.CreateText(fileName))
            {
                streamWriter.WriteLine(serializedData);
                streamWriter.Close();
            }
        }

        public static string readingDataFromFile(string filename) {
            
            string fileData;
            FileStream fileStream = File.OpenRead(filename);
            using (StreamReader streamReader = new StreamReader(fileStream))
            {
                fileData = streamReader.ReadLine();
                fileStream.Close();
            }
            return fileData;
        }
    }
}
