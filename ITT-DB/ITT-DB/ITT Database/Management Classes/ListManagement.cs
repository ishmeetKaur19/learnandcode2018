namespace ITT_Database
{
    #region Namespaces

    using System;
    using System.IO;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using Newtonsoft.Json.Linq;
    using System.Reflection;

    #endregion Namespaces

    
    class ListManagement {
        
        public static void setIdOfNewObject(List<object> objectList, object newObjectToStore)
        {
            PropertyInfo objectProperty = newObjectToStore.GetType().GetProperty("id");
            if (objectList.Count == 0)
            {
                objectProperty.SetValue(newObjectToStore, 1, null);
            }
            else
            {
                Object lastObject = objectList[objectList.Count - 1];
                JObject jObject = JObject.FromObject(lastObject);
                int id = (int)jObject["id"];
                objectProperty.SetValue(newObjectToStore, id + 1, null);
            }

        }

        public static List<object> appendObjectListWithNewObject(List<object> objectList, object newObjectToBeStored)
        {
            objectList.Add(newObjectToBeStored);
            return objectList;
        }

        public static List<object> removeObjectFromObjectList(List<object> objectList, object objectToBeRemoved) {
            objectList.Remove(objectToBeRemoved);
            return objectList;

        }


    } 
}