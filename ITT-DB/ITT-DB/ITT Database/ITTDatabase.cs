﻿
namespace ITT_Database
{
    #region Namespaces

    using System;
    using System.IO;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using Newtonsoft.Json.Linq;
    using ITT_Database;

    #endregion Namespaces

    /// <summary>
    /// Class ITTDatabase contains CURD operations.
    /// </summary>
    public class ITTDatabase : IITTDatabase
    {
        ObjectManagement objectManagement = new ObjectManagement();
        
        public bool Save(Object obj)
        {

            try
            {
                if (obj == null)
                {
                    throw new ArgumentNullException("objectToBeStore", "object to be stored sholuld not be null");
                }

                objectManagement.saveObject(obj);
                return true;
            }

            catch (Exception exception)
            {
                return false;
                throw new ITTDatabaseException("SAVE_ERROR", exception);
            }

        }

        public object Find(object instance, string key, string expectedValue)
        {

            try
            {
                if (instance == null)
                {
                    throw new ArgumentNullException("instance", "object should not be null");
                }

                return objectManagement.findObject(instance, key, expectedValue);
            }

            catch (Exception exception)
            {
                throw new ITTDatabaseException("FIND_ERROR", exception);
            }

        }

        public bool Delete(object instance, int id)
        {

            try
            {
                if (instance == null)
                {
                    throw new ArgumentNullException("instance", "object should not be null");
                }

                return objectManagement.deleteObject(instance, id);
            }

            catch (Exception exception)
            {
                throw new ITTDatabaseException("DELETE_ERROR", exception);
            }
        }

        public bool Modify(object obj)
        {
            
            try
            {
                if (obj == null)
                {
                    throw new ArgumentNullException("instance", "object should not be null");
                }
                return objectManagement.modifyObject(obj);
                
            }

            catch (Exception exception)
            {
                throw new ITTDatabaseException("MODIFY_ERROR", exception);
            }
        }
    }
}


