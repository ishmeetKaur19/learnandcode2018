
namespace ITT_Database
{
    #region Namespaces

    using System;

    #endregion Namespaces

    
    class ITTDatabaseException : Exception
    {
        
        public ITTDatabaseException()
        : base()
        { }

        public ITTDatabaseException(string errorCode, Exception exception)
        {
            switch (errorCode)
            {
                case "SAVE_ERROR":
                    Console.WriteLine("Object is not saved in database, see the deatils {0}", exception.Message);
                    break;

                case "FIND_ERROR":
                    Console.WriteLine("Object is not found in the database, see the details {0}", exception.Message);
                    break;

                case "DELETE_ERROR":
                    Console.WriteLine("Object is not deleted successfully, see the deatils {0}", exception.Message);
                    break;

                case "MODIFY_ERROR":
                    Console.WriteLine("Object is not modified in the databse successfully {0}", exception.Message);
                    break;

            }
        }
    }
}