namespace ITT_Database 
{
    /// <summary>
    /// interface which defines all the methods of database
    /// </summary>
    public interface IITTDatabase
    {
        bool Save(object obj);
        object Find(object obj, string key, string expectedValue);
        bool Delete(object obj, int id);
        bool Modify(object obj);
    }
}
